<?php

namespace App\DataFixtures;

use App\Entity\Movies;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {

        $time = [
            '1:58',
            '1:25',
            '1:47',
            '2:22',
            '4:58',
        ];
        $faker = Factory::create();
        for ($i = 0; $i < 5; $i++){
            $movie = new Movies();
            $movie->setTitre($faker->name())
                ->setDescription($faker->realText())
                ->setConseil($faker->paragraph(2))
                ->setImage($faker->file('public/images/test', 'public/images/movies'))
                ->setShowTime($time[$i])
                ->setNote(mt_rand(0, 5))
                ->setYearAt($faker->dateTimeBetween('-6 months'))
                ->setAddAt(new \DateTime());

             $manager->persist($movie);
        }
        $manager->flush();
    }
}
