<?php

namespace App\Form;

use App\Entity\Category;
use App\Entity\Movies;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;


class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('titre')
            ->add('description')
            ->add('conseil')
            ->add('category', EntityType::class, [
                'class' => Category::class,
                'choice_label' => 'name',
                ])
            ->add('yearAt')
            ->add('imageFile', VichImageType::class)
            ->add('note')
            ->add('showTime');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Movies::class,
        ]);
    }
}
