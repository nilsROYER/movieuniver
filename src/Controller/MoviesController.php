<?php

namespace App\Controller;

use App\Entity\Movies;
use App\Form\MovieType;
use App\Repository\MoviesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MoviesController extends AbstractController
{
    /**
     * @Route("/movies", name="movies")
     */
    public function index(MoviesRepository $repo)
    {
        if(!$this->isGranted('ROLE_VALID')){
            $this->addFlash('warning', 'Inscrivez vous et validé votre compte pour voir les film');
            return $this->redirectToRoute('home');
        }
        // On va chercher tout les film
        $movies = $repo->findAll();
        return $this->render('movies/allMovies.html.twig', [
            'movies' => $movies,
        ]);
    }

    /**
     * @Route("/movie/{id}[0-9]+", name="select_movie")
     */
    public function showOne(Movies $movie)
    {
        return $this->render('movies/movie.html.twig', [
            'movie' => $movie
        ]);
    }

    /**
     * @Route("/movie/new", name="add_movie")
     */
    public function add(Request $request, EntityManagerInterface $manager)
    {
        if ( ! $this->isGranted('ROLE_ADMIN')){
            return $this->redirectToRoute('home');
        }
        $movie = new Movies();
        // On appel le formulaire de films
        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);
        // Si le formulaire et soumis et est valid
        if ($form->isSubmitted() && $form->isValid()) {
            // On enregistre le nouveau film
            $manager->persist($movie);
            // On enregistre en base de données le nouveau film
            $manager->flush();

            return $this->redirectToRoute('select_movie', ['id' => $movie->getId()]);
        }
        return $this->render('movies/add-movie.html.twig', [
            'movieForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/movie/update/{id}[0-9]+", name="update_movie")
     */
    public function update(Movies $movie, Request $request, EntityManagerInterface $manager)
    {

        if ( ! $this->isGranted('ROLE_ADMIN')){
            return $this->redirectToRoute('home');
        }

        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($movie);
            $manager->flush();
            return $this->redirectToRoute('select_movie', ['id' => $movie->getId()]);
        }
        return $this->render("movies/update.html.twig", [
            'updateForm' => $form->createView(),
        ]);
    }
}
