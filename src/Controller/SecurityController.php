<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ResetPassType;
use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/oubli-pass", name="app_forgotten_password")
     */
    public function forgottenPass(Request $request, UserRepository $userRepo, \Swift_Mailer $mailer, TokenGeneratorInterface $tokenGenerator)
    {
        // On créer le formulaire
        $form = $this->createForm(ResetPassType::class);
        // On traite le formulaire
        $form->handleRequest($request);
        // si le formulaire est valide
        if ($form->isSubmitted() && $form->isValid()) {
            $donnees = $form->getData();

            // On cherche si un utilisateur a cet email
            $user = $userRepo->findOneBy(['email' => $donnees['email']]);
//            dd($donnees);

            // Si l'utilisateur n'éxiste pas
            if (!$user) {
                // On envoie un message flash
                $this->addFlash('danger', 'cette adrresse n\'éxiste pas');
                return $this->redirectToRoute('app_login');
            }

            // On génére un token
            $token = $tokenGenerator->generateToken();


            try {
                $user->setResetToken($token);
                $manager = $this->getDoctrine()->getManager();
                $manager->persist($user);
                $manager->flush();
            } catch (\Exception $exception) {
                $this->addFlash('warning', 'Une erreur est survenue : ' . $exception->getMessage());
                return $this->redirectToRoute('app_login');
            }

            // On génére l'URL de réinitilisation de mot de passe
            $url = $this->generateUrl('app_reset_password', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);

            // On envoie un mail pour vérifier le compte le mail contien un lien d'activation
            // On créer le message
            $message = (new \Swift_Message('Mot de passe oublié'))
                // On attribue l'ecpéditeur le site
                ->setFrom('site@site.fr')
                // On attribue le destinataire
                ->setTo($user->getEmail())
                // On créer le contenue
                ->setBody(
                    $this->renderView('email/resetPass.html.twig', ['url' => $url]),
                    'text/html'
                );

            // On envoie le message dans un mail
            $mailer->send($message);

            // On créer le message flash
            $this->addFlash('message', 'un e-mail de réinitialisation de mot de passe vous a été envoyé');
            return $this->redirectToRoute('app_login');
        }

        // On envoie vers la page de demande de l'e-mail
        return $this->render('security/forgotten_password.html.twig', ['emailForm' => $form->createView()]);
    }

    /**
     * @Route("/reset-pass/{token}", name="app_reset_password")
     */
    public function resetPassword($token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        // On cherche l'utilisateur avec le token fourni
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['reset_token' => $token]);

        if (!$user) {
            $this->addFlash('danger', 'Token inconnu');
            return $this->redirectToRoute('pp_login');
        }

        // Si le formulaire est envoyé en mméthode POST
        if ($request->isMethod('POST')) {
            // On supprime le token
            $user->setResetToken(null);

            // On chiffre le mot de passe
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));

            $manager = $this->getDoctrine()->getManager();
            $manager->persist($user);
            $manager->flush();

            $this->addFlash('message', 'Mot de passe modifié avec succès');
            return $this->redirectToRoute('app_login');
        } else {
            return $this->render('security/reset_password.html.twig', [
                'token' => $token,
            ]);
        }
    }
}