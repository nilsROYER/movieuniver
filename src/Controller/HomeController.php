<?php

namespace App\Controller;

use App\Entity\Category;
use App\Repository\CategoryRepository;
use App\Repository\MoviesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(CategoryRepository $repo)
    {
        // On va chercher tout les film
        $categories = $repo->findAll();
//        dd($categories);
        return $this->render('home/index.html.twig', [
            'categories' => $categories,
        ]);
    }

    /**
     * @Route("/category/{id}[0-9]+", name="select_by_category")
     */
    public function selectCategory(Category $category)
    {

        if(!$this->isGranted('ROLE_VALID')){
            $this->addFlash('warning', 'Inscrivez vous et validé votre compte pour voir les film');
            return $this->redirectToRoute('home');
        }
        return $this->render('/movies/category/category_movies.html.twig', [
            'category' => $category,
        ]);
    }

    /**
     * @Route("/master", name="app_admin")
     */
    public function admin()
    {
        return $this->redirect('/admin');
    }

}
