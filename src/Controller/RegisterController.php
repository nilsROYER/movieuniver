<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\RegisterType;
use App\Repository\UserRepository;
use App\Security\Authenticator;
use Doctrine\ORM\EntityManagerInterface;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="app_register")
     *
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @param UserPasswordEncoderInterface $encoder
     * @param Authenticator $authenticator
     * @param GuardAuthenticatorHandler $guardHandler
     * @return Response
     */
    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder,
                                  \Swift_Mailer $mailer): Response
    {
        $user = new User();
        $form = $this->createForm(RegisterType::class, $user);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // On chiffre le mot de passe
            $hash = $encoder->encodePassword($user, $user->getPassword());
            // On remplace le mot de passe par sa version chiffré
            $user->setPassword($hash);

            // On génère le token d'activation
            $user->setActivationToken(md5(uniqid())); // md5 : algorithme de chiffrement, uniqid : algorithm pour avoir des id unique
            $manager->persist($user);
            $manager->flush();

            // On envoie un mail pour vérifier le compte le mail contien un lien d'activation
            // On créer le message
            $message = (new \Swift_Message('Activation de votre compte'))
                // On attribue l'ecpéditeur le site
                ->setFrom('site@site.fr')
                // On attribue le destinataire
                ->setTo($user->getEmail())
                // On créer le contenue
                ->setBody(
                    $this->renderView('email/activation.html.twig', ['token' => $user->getActivationToken()]),
                    'text/html'
                );
            // On envoie le message dans un mail
            $mailer->send($message);
            $this->addFlash('message', 'Le message à bien était envoyer');

            return $this->redirectToRoute('home');

        }

        return $this->render("register/register.html.twig", [
            'registerForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/activation/{token}", name="activation")
     */
    public function activation($token, UserRepository $userRepo, Authenticator $authenticator, GuardAuthenticatorHandler $guardHandler, Request $request)
    {
        // On vérifie si l'utilisateur a ce token
        $user = $userRepo->findOneBy(['activation_token' => $token]);

        // Si aucun utilisateur existe avec ce token
        if (!$user) {
            // error 404
            throw $this->createNotFoundException("Cet utilisateur n'existe pas");
        }
        $user->setActivationToken(null);
        $manager = $this->getDoctrine()->getManager();
        $user->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);
        $manager->flush();

        // On envoie un message Flash
        $this->addFlash('success', 'Vous avez bien activé votre compte');

        // On redirige vers l'accueil
//        return $this->redirectToRoute('home');
        return $guardHandler->authenticateUserAndHandleSuccess(
            $user,
            $request,
            $authenticator,
            'main'
        );
    }

}
